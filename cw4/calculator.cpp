#include <cstdlib>
#include <cmath>
#include <string>
#include <sstream>
#include "calculator.h"

using namespace calculator;

// Mathematical functions //

double addition(double a, double b) { return a + b; }

double subtraction(double a, double b) { return a - b; }

double multiplication(double a, double b) { return a * b; }

double division(double a, double b)
{
    if (b == 0) throw MathException();

    return a / b;
}

// Getting help from std, because it's so much easier than implementing power
// on our own.
double power(double a, double b) { return std::pow(a, b); }

////////////////////////////////////////////
// Actual Calculator class implementation //
////////////////////////////////////////////

Calculator::Calculator() : num_stack(stack::Stack<double>()), running_(true)
{
    register_function(addition, std::string("+"), 3);
    register_function(subtraction, std::string("-"), 3);
    register_function(multiplication, std::string("*"), 2);
    register_function(division, std::string("/"), 2);
    register_function(power, std::string("^"), 1);
}

Calculator::~Calculator()
{
    functions.clear();
}

void Calculator::register_function(math_function fnc, const std::string name,
                                   int priority)
{
    // In actual case, when we deal with infix notation, we should use priority
    // for defining which mathematical functions are more important.
    // BUT! We're dealing with postfix notation, so we don't really care.
    // However I decided to leave this argument for future use (if there's
    // gonna be any at all).

    functions[name] = fnc;
}

bool Calculator::running()
{
    return running_;
}

// Take input in post-fix format and compute the result.
double Calculator::compute(std::string input)
{
    if (input == "q")
    {
        running_ = false;
        return 0.0;
    }

    std::stringstream stream(input);
    std::string tmp_str;

    double operand;
    std::string operator_;

    while (stream >> tmp_str)
    {
        operand = std::atof(tmp_str.c_str());

        // There actually does not exist any other method of checking if string
        // converted to number correctly.
        std::stringstream stream_tmp;
        stream_tmp << operand;

        // WARNING: This is not perfect!  There're some issues with precision!
        if (stream_tmp.str() == tmp_str)
        {
            // operand is a number
            num_stack.push(operand);
        }
        else
        {
            // no! We found an operator
            operator_ = tmp_str;

            double arg2 = num_stack.pop();
            double arg1 = num_stack.pop();

            // We're taking function by it's name, executing with specified
            // arguments and pushing the result back to the stack.
            FunctionsMap::iterator fnc_ptr = functions.find(operator_);
            if (fnc_ptr != functions.end())
                num_stack.push( (*fnc_ptr->second)(arg1, arg2) );
            else
                throw WrongExpression();
        }
    }

    double result = num_stack.top();
    num_stack.clear();
    return result;
}
