#include <iostream>
#include <fstream>
#include "stack.h"
#include "calculator.h"

using namespace std;
using namespace stack;
using namespace calculator;

int main(int argc, char const *argv[])
{
    Calculator calc = Calculator();
    string expression;
    bool running = true;
    double result = 0;

    if (argc > 1)
    {
        // read data from files instead of STDIN
        for (int i = 1; i < argc; ++i)
        {
            // open file from argv[i]
            ifstream file(argv[i]);

            if (file.good())
            {
                // read whole line while there's still some lines left
                while (getline(file, expression))
                {
                    try
                    {
                        result = calc.compute(expression);
                    }
                    catch (exception &e)
                    {
                        cout << "UWAGA: wyrazenie nie jest poprawne!" << endl
                             << "Komunikat bledu: " << e.what() << endl
                             << endl;
                        continue;
                    }

                    cout << "Wynik: " << result << endl;
                }
            }
            else
            {
                cerr << "Nie mozna otworzyc pliku '" << argv[i] << "'!"
                     << endl;
            }

            // close the file
            file.close();
        }
    }
    else
    {
        // read from STDIN
        while (running)
        {
            cout << "Podaj wyrazenie ['q' aby wyjsc]:" << endl;
            cout << ">>> ";
            getline(cin, expression);

            try
            {
                result = calc.compute(expression);
            }
            catch (exception &e)
            {
                cout << "UWAGA: wyrazenie nie jest poprawne!" << endl;
                cout << "Komunikat bledu: " << e.what() << endl << endl;
                continue;
            }

            if (!calc.running())
            {
                cout << "Do nastepnego razu!" << endl;
                break;
            }

            cout << "Wynik: " << result << endl;
        }
    }

    return 0;
}
