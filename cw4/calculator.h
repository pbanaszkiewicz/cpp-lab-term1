#ifndef _CALCULATOR_H
#define _CALCULATOR_H 1

#include <exception>
#include <map>
#include <string>
#include "stack.h"

namespace calculator
{
    // math_function is a pointer to the function returning double and taking
    // two double arguments.
    typedef double (*math_function)(double, double);
    typedef std::map<std::string, math_function> FunctionsMap;

    class MathException: public std::exception
    {
        virtual const char *what() const throw()
        {
            return "This is mathematically forbidden!";
        }
    };

    class WrongExpression: public std::exception
    {
        virtual const char *what() const throw()
        {
            return "This expression is not correct!";
        }
    };

    class Calculator
    {
    public:
        Calculator();
        ~Calculator();

        void register_function(math_function, const std::string, int);

        bool running();
        double compute(std::string);

    private:
        FunctionsMap functions;
        stack::Stack<double> num_stack;
        bool running_;
    };
}

#endif
