#ifndef _STACK_H_
#define _STACK_H_ 1

#include <exception>
#include <algorithm>

namespace stack
{
    // Exception used when someone wants to peek at stack's top element, but
    // the stack is empty.
    class StackEmptyException: public std::exception
    {
        virtual const char *what() const throw()
        {
            return "The stack is empty!";
        }
    };

    // Templates help making stack more generic -- it can now store not only
    // integers, but also whatever type you'd think of.
    template <typename T>
    class Stack
    {
    public:
        Stack();
        ~Stack();

        T& top();
        void push(T);
        T& pop();

        unsigned int size();
        bool is_empty();

        void clear();

    private:
        static const int _beginning_size = 20;
        static const int increment = 10;
        int _size;
        int top_index;
        T *data;
    };
}


// IMPLEMENTATION DETAILS //

// We store implementation in this file, not in separate one, due to
// templating.  The compiler needs to have implementation details in the same
// file, it's mandatory.


template <typename T>
stack::Stack<T>::Stack()
{
    // default values
    _size = _beginning_size;
    top_index = -1;
    data = new T[_size];
}


template <typename T>
stack::Stack<T>::~Stack()
{
    delete [] data;
}


// Return stack's top value if it exists, throw exception otherwise.
template <typename T>
T& stack::Stack<T>::top()
{
    if (top_index < 0)
        throw StackEmptyException();

    return data[top_index];
}


// Put new value on top of the stack.  Expand the stack if necessary.
template <typename T>
void stack::Stack<T>::push(T item)
{
    if (_size - top_index < 2)
    {
        // expanding requires copying the data manually to new location, then
        // pointing this->data there
        T *new_data = new T[_size + increment];
        std::copy(data, data + increment, new_data);

        // ... and finally removing old data
        delete [] data;
        data = new_data;
        new_data = NULL;

        _size += increment;
    }

    data[++top_index] = T(item);
}


// Take value from the top of the stack and return it.
template <typename T>
T& stack::Stack<T>::pop()
{
    if (top_index < 0)
        throw StackEmptyException();

    return data[top_index--];
}


// Return actual stack size (number of items stored).
template <typename T>
unsigned int stack::Stack<T>::size()
{
    return top_index + 1;
}


// Return positive value if the stack is empty, negative otherwise.
template <typename T>
bool stack::Stack<T>::is_empty()
{
    return top_index < 0;
}


// Make the stack empty.
template <typename T>
void stack::Stack<T>::clear()
{
    delete [] data;
    top_index = -1;
    _size = _beginning_size;
    data = new T[_size];
}

#endif
