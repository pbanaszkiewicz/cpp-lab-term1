#ifndef _MATRICES_H_
#define _MATRICES_H_ 1

#include <exception>
#include <algorithm>
#include <iostream>
#include "config.h"

typedef unsigned int uint;

namespace matrices
{
    class wrong_size_exc : public std::exception
    {
        virtual const char* what() const throw()
        {
            return "Dimensions don't match!";
        }
    };

    class no_null_pointers : public std::exception
    {
        virtual const char* what() const throw()
        {
            return "Some pointers should be NULL!";
        }
    };

    class vector
    {
    public:
        vector(uint size, int default_value) : size(size), array(NULL)
        {
            fill(size, default_value);
        }
        vector() : size(0), array(NULL) {}
        ~vector()
        {
            delete[] array;
            array = NULL;
        }

        vector(const vector &w)
        {
            size = w.size;
            array = new int[size];
            for (int i = 0; i < w.size; ++i)
            {
                array[i] = w.array[i];
            }
        }

        // unfortunately this operator is needed, because copying constructor
        // alone doesn't work
        vector& operator=(const vector&);

        void fill(uint size, int default_value);
        int& operator[](size_t);
        int& operator[](size_t) const;
        bool operator==(const vector&);
        bool operator!=(const vector&);

        vector& operator+(const vector&);
        vector& operator-(const vector&);
        vector& operator+=(const vector&);
        vector& operator-=(const vector&);
        vector& operator*(const int&);
        vector& operator*=(const int&);

        friend std::ostream& operator<<(std::ostream &output,
                                        const vector &w)
        {
            output << "[";

            for (int x = 0; x < w.get_size(); ++x)
            {
                output << " " << w[x];
            }

            output << " ]";
            return output;
        }

        friend std::istream& operator>>(std::istream &input, vector &v)
        {
            uint size;
            int def_val;

            input >> size;
            input >> def_val;
            v.fill(size, def_val);

            return input;
        }

        uint get_size() const { return size; }

    private:
        int *array;
        uint size;
    };


    class matrix
    {
    public:
        matrix(uint size_y, uint size_x, int default_value) :
            size_x(size_x), size_y(size_y)
        {
            fill(size_y, size_x, default_value);
        }

        matrix() : size_x(0), size_y(0), vectors(NULL) { }

        ~matrix()
        {
            delete[] vectors;
            vectors = NULL;
        }

        matrix(const matrix &m)
        {
            size_y = m.size_y;
            size_x = m.size_x;
            vectors = new vector[size_y];
            for (int i = 0; i < size_y; ++i)
            {
                vectors[i] = m.vectors[i];
            }
        }

        // unfortunately this operator is needed, because copying constructor
        // alone doesn't work
        matrix& operator=(const matrix&);

        void fill(uint size_y, uint size_x, int default_value);

        vector& operator[](size_t);
        vector& operator[](size_t) const;
        bool operator==(const matrix&);
        bool operator!=(const matrix&);

        matrix& operator+(const matrix&);
        matrix& operator-(const matrix&);
        matrix& operator+=(const matrix&);
        matrix& operator-=(const matrix&);
        matrix& operator*(const matrix&);
        matrix& operator*(const int&);
        matrix& operator*=(const matrix&);
        matrix& operator*=(const int&);

        friend std::ostream& operator<<(std::ostream &output,
                                        const matrix &m)
        {
            if (DEBUG)
                output << "size: [" << m.get_size_y() << ", "
                       << m.get_size_x() << "]" << std::endl;

            for (int y = 0; y < m.get_size_y(); ++y)
            {
                for (int x = 0; x < m.get_size_x(); ++x)
                {
                    output << m[y][x] << " ";
                }
                output << std::endl;
            }
            return output;
        }

        friend std::istream& operator>>(std::istream &input, matrix &m)
        {
            uint size_y, size_x;
            int def_val;

            input >> size_y;
            input >> size_x;
            input >> def_val;
            m.fill(size_y, size_x, def_val);

            return input;
        }

        uint get_size_x() const { return size_x; }
        uint get_size_y() const { return size_y; }

    private:
        vector *vectors;
        uint size_x;
        uint size_y;
    };
}

#endif
