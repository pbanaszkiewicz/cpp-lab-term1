#include "matrices.h"
using namespace matrices;

vector& vector::operator=(const vector &w)
{
    size = w.size;
    array = new int[size];
    for (int i = 0; i < size; ++i)
    {
        array[i] = w.array[i];
    }
    return *this;
}

void vector::fill(uint size=2, int default_value=0)
{
    this->size = size;

    array = new int[size];
    std::fill(array, array + size, default_value);
}

int& vector::operator[](size_t n)
{
    return array[n];
}

int& vector::operator[](size_t n) const
{
    return array[n];
}

bool vector::operator==(const vector &w)
{
    for (int i = 0; i < size; ++i)
    {
        if (array[i] != w.array[i])
            return false;
    }
    return true;
}

bool vector::operator!=(const vector &w)
{
    for (int i = 0; i < size; ++i)
    {
        if (array[i] != w.array[i])
            return true;
    }
    return false;
}

vector& vector::operator+(const vector &w)
{
    *this += w;
    return *this;
}

vector& vector::operator-(const vector &w)
{
    *this -= w;
    return *this;
}

vector& vector::operator+=(const vector &w)
{
    if (size != w.size)
        throw wrong_size_exc();

    for (int i = 0; i < w.size; ++i)
        array[i] += w.array[i];

    return *this;
}

vector& vector::operator-=(const vector &w)
{
    if (size != w.size)
        throw wrong_size_exc();

    for (int i = 0; i < w.size; ++i)
        array[i] -= w.array[i];
    return *this;
}

vector& vector::operator*(const int &i)
{
    *this *= i;
    return *this;
}

vector& vector::operator*=(const int &i)
{
    for (int j = 0; j < size; ++j)
    {
        array[j] *= i;
    }
    return *this;
}

///////////////////////////////////////////////////////////////////////////////

matrix& matrix::operator=(const matrix &m)
{
    size_y = m.size_y;
    size_x = m.size_x;
    vectors = new vector[size_y];
    for (int i = 0; i < size_y; ++i)
    {
        vectors[i] = m.vectors[i];
    }
    return *this;
}

void matrix::fill(uint size_y=2, uint size_x=2, int default_value=2)
{
    // if (vectors != NULL)
    //     throw no_null_pointers();

    this->size_y = size_y;
    this->size_x = size_x;
    vectors = new vector[size_y];
    for (int i = 0; i < size_y; ++i)
    {
        vectors[i].fill(size_x, default_value);
    }
}

vector& matrix::operator[](size_t n)
{
    return vectors[n];
}

vector& matrix::operator[](size_t n) const
{
    return vectors[n];
}

bool matrix::operator==(const matrix &m)
{
    for (int i = 0; i < size_y; ++i)
    {
        if (vectors[i] != m.vectors[i])
            return false;
    }
    return true;
}

bool matrix::operator!=(const matrix &m)
{
    for (int i = 0; i < size_y; ++i)
    {
        if (vectors[i] != m.vectors[i])
            return true;
    }
    return false;
}

matrix& matrix::operator+(const matrix &m)
{
    *this += m;
    return *this;
}

matrix& matrix::operator-(const matrix &m)
{
    *this -= m;
    return *this;
}

matrix& matrix::operator+=(const matrix &m)
{
    if (size_x != m.size_x || size_y != m.size_y)
        throw wrong_size_exc();

    for (int i = 0; i < m.size_y; ++i)
        vectors[i] += m.vectors[i];
    return *this;
}

matrix& matrix::operator-=(const matrix &m)
{
    if (size_x != m.size_x || size_y != m.size_y)
        throw wrong_size_exc();

    for (int i = 0; i < m.size_y; ++i)
        vectors[i] -= m.vectors[i];
    return *this;
}

matrix& matrix::operator*(const matrix &m)
{
    *this *= m;
    return *this;
}

matrix& matrix::operator*(const int &i)
{
    *this *= i;
    return *this;
}

matrix& matrix::operator*=(const matrix &m)
{
    if (size_x != m.size_y)
    {
        throw wrong_size_exc();
    }

    uint new_size_y = size_y, new_size_x = m.size_x;

    matrix *tmp = new matrix(new_size_y, new_size_x, 0);

    // ordinary matrix multiplication algorithm
    for (int y = 0; y < new_size_y; ++y)
    {
        for (int x = 0; x < new_size_x; ++x)
        {
            uint sum = 0;
            for (int i = 0; i < size_x; ++i)
            {
                sum += (*this)[y][i] * m[i][x];
            }
            (*tmp)[y][x] = sum;
        }
    }

    // I don't want to return a new object, I want to return *this. And thus
    // I need to replace this->vectors with tmp->vectors
    delete[] vectors;
    size_y = new_size_y; size_x = new_size_x;
    vectors = tmp->vectors;

    tmp->vectors = NULL;
    delete tmp;

    return *this;
}

matrix& matrix::operator*=(const int &i)
{
    for (int j = 0; j < size_y; ++j)
    {
        vectors[j] *= i;
    }
    return *this;
}
