#include <iostream>
#include <cstdlib>
#include "config.h"
#include "matrices.h"
using namespace std;
using namespace matrices;

const int rand_range = 20;

int main(int argc, char const *argv[])
{
    srand(time(NULL));

    cout << "###############" << endl
         << "TESTING VECTORS" << endl
         << "###############" << endl;

    vector v1;
    v1.fill(4, 10);
    v1[0] = rand() % rand_range;
    v1[3] = rand() % rand_range;
    cout << endl << "Vector v1:" << endl;
    cout << v1 << endl;

    vector v2(4, 10);
    cout << endl << "Vector v2:" << endl;
    cout << v2 << endl;

    vector v3 = v2 + v1;
    cout << endl << "Vector v3 = v2 + v1:" << endl;
    cout << v2 << endl << "  +" << endl << v1 << endl << "  =" << endl;
    cout << v3 << endl;

    vector v4 = v1 * 4;
    cout << endl << "Vector v4 = v1 * 4:" << endl;
    cout << v1 << endl << "  *" << endl << 4 << endl << "  =" << endl;
    cout << v4 << endl;

    cout << endl << "Comparing vectors: v1 == v1" << endl;
    cout << v1 << endl << "  ==" << endl << v1 << endl << "  =" << endl;
    cout << (v1 == v1) << endl;

    cout << endl << "Comparing vectors: v3 != v4" << endl;
    cout << v3 << endl << "  !=" << endl << v4 << endl << "  =" << endl;
    cout << (v3 != v4) << endl;

    cout << endl << "Testing input operators. Please enter size and default"
         " value below:" << endl;
    vector v5;
    cin >> v5;
    cout << "Your brand new vector v5:" << endl;
    cout << v5 << endl << endl;

    //////////////////////////////////////////

    cout << "################" << endl
         << "TESTING MATRICES" << endl
         << "################" << endl;

    matrix m1(3, 2, -10);
    cout << endl << "Matrix m1:" << endl;
    cout << m1;

    matrix m2(2, 2, 0);
    m2[0][0] = 1;
    m2[1][1] = 1;
    m2[0][1] = 1;
    cout << endl << "Matrix m2:" << endl;
    cout << m2;

    matrix m3 = m2 * 2;
    cout << endl << "Matrix m3 = m2 * 2:" << endl;
    cout << m2 << "  *" << endl << 2 << endl << "  =" << endl;
    cout << m3;

    matrix m4 = m1 * m3;
    cout << endl << "Matrix m4 = m1 * m3:" << endl;
    cout << m1 << "  *" << endl << m3 << "  =" << endl;
    cout << m4;

    matrix m5 = m4 - m1;
    cout << endl << "Matrix m5 = m4 * m1:" << endl;
    cout << m4 << "  -" << endl << m1 << "  =" << endl;
    cout << m5;

    cout << endl << "Comparing matrices: m3 == m2" << endl;
    cout << m3 << "  ==" << endl << m2 << "  =" << endl;
    cout << (m3 == m2) << endl;

    cout << endl << "Testing input operators. Please enter size_y, size_x,"
         " and default value below:" << endl;
    matrix m6;
    cin >> m6;
    cout << "Your brand new matrix m6:" << endl;
    cout << m6;

    return 0;
}
