#include <stdlib.h>
#include <stdio.h>
#include "binary_tree.h"


/********
 * MAIN *
 ********/

int main(int argc, char const *argv[])
{
    printf("#### K S I A Z K A  T E L E F O N I C Z N A ####\n");
    // Node *root = new_node(0);
    Node *root = NULL;
    Node *tmp_node = NULL;
    Entry *tmp_entry = NULL;

    // menu
    short working = 1;
    int action = 0;
    while (working)
    {
        printf("1) Wypisz dane na ekranie (metoda in-order)\n");
        printf("2) Wstaw nowy wpis\n");
        printf("3) Znajdz minimum\n");
        printf("4) Znajdz maksimum\n");
        printf("5) Podaj ilosc wezlow w drzewie\n");
        printf("6) Znajdz okreslony klucz w drzewie\n");
        printf("7) Podaj wysokosc drzewa\n");
        printf("0) Zakoncz program\n");
        printf(">>> ");
        scanf("%d", &action);
        printf("\n");

        switch (action)
        {
            case 1:
                tree_LVR(root);
                break;

            case 2:
                tmp_node = new_node(1);
                insert_node(&root, tmp_node);
                tmp_node = NULL;
                break;

            case 3:
                tmp_entry = tree_min(root);
                print_key(tmp_entry);
                tmp_entry = NULL;
                break;

            case 4:
                tmp_entry = tree_max(root);
                print_key(tmp_entry);
                tmp_entry = NULL;
                break;

            case 5:
                {
                    unsigned int tmp = tree_count_nodes(root);
                    printf("# Wezly: %u\n", tmp);
                    break;
                }

            case 6:
                tmp_entry = new_entry();
                tmp_node = tree_search(root, tmp_entry);

                if (tmp_node == NULL)
                    printf("# NIE ZNALEZIONO!\n");
                else
                    printf("# Klucz odnaleziony!\n");
                tmp_node = NULL;

                free(tmp_entry);
                tmp_entry = NULL;
                break;

            case 7:
                {
                    unsigned int tmp = tree_height(root);
                    printf("# Wysokosc drzewa: %u\n", tmp);
                    break;
                }

            default:
                working = 0;
                break;
        }
    }

    clean_tree(root);
    return 0;
}
