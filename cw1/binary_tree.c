#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "binary_tree.h"

/******************
 * IMPLEMENTATION *
 ******************/

// return new Entry (fields entered by human)
Entry *new_entry()
{
    Entry *osoba = (Entry*) malloc(sizeof(Entry));

    printf("# Podaj imie: ");
    scanf("%s", osoba->imie);

    printf("# Podaj nazwisko: ");
    scanf("%s", osoba->nazwisko);

    printf("# Podaj 1. numer telefonu: ");
    scanf("%lu", &(osoba->telefon1));

    printf("# Podaj 2. numer telefonu: ");
    scanf("%lu", &(osoba->telefon2));

    printf("# Podaj 3. numer telefonu: ");
    scanf("%lu", &(osoba->telefon3));

    return osoba;
}

// return new Node (containing user-entered data for node's key)
Node *new_node(short enter_values)
{
    Node *n = (Node*) malloc(sizeof(Node));
    n->left = NULL;
    n->right = NULL;
    // n->left = (Node*) malloc(sizeof(Node));
    //     n->left->key = NULL;
    //     n->left->left = NULL;
    //     n->left->right = NULL;
    // n->right = (Node*) malloc(sizeof(Node));
    //     n->right->key = NULL;
    //     n->right->left = NULL;
    //     n->right->right = NULL;

    if (enter_values)
        n->key = new_entry();
    else
        n->key = NULL;

    return n;
}

// return -1 if L<R, 0 for L==R and 1 for L>R
int entry_cmp(Entry *left, Entry *right)
{
    char str1[2 * (SIZE + 1)];
    char str2[2 * (SIZE + 1)];

    // from {"nazwisko": "Nowak", "imie": "Adam"} create str1 == "NowakAdam";
    strcpy(str1, left->nazwisko);
    strcat(str1, left->imie);

    strcpy(str2, right->nazwisko);
    strcat(str2, right->imie);

    return -strcmp(str1, str2);
}

// insert node into the binary tree
short insert_node(Node **root, Node *n)
{
    Node **tree_root = root;

    while (1)
    {
        // if the root is empty, we insert the very first element into the tree
        if (*tree_root == NULL)
        {
            *tree_root = new_node(0);
            (*tree_root)->key = n->key;
            free(n);
            n = NULL;
            return 1;
        }

        // we switch to the next (deeper) level
        int cmp_result = entry_cmp((*tree_root)->key, n->key);
        if (cmp_result < 0)
            tree_root = &(*tree_root)->left;
        else
            tree_root = &(*tree_root)->right;
    }

    return 0;
}

// represent key to the STDOUT
void print_key(Entry *key)
{
    printf("# %s, %s: %lu, %lu, %lu\n", key->nazwisko, key->imie,
           key->telefon1, key->telefon2, key->telefon3);
}

// print tree in alphabetical order
void tree_LVR(Node *root)
{
    if (!root || !root->key)
        return;

    tree_LVR(root->left);
    print_key(root->key);
    tree_LVR(root->right);
}

// find minimal value in the tree
Entry* tree_min(Node *root)
{
    if (!root->left) return root->key;
    else return tree_min(root->left);
}

// find minimal value in the tree
Entry* tree_max(Node *root)
{
    if (!root->right) return root->key;
    else return tree_max(root->right);
}

// count the number of nodes in the tree
unsigned int tree_count_nodes(Node *root)
{
    unsigned int sum = 0;
    if (root->left)
        sum += tree_count_nodes(root->left);

    if (root->right)
        sum += tree_count_nodes(root->right);

    return ++sum;
}

// search tree for given key (exact match needed)
Node* tree_search(Node *root, Entry *key)
{
    int cmp_result = entry_cmp(root->key, key);

    if (cmp_result < 0)
    {
        // switch to the left branch (if it exists)
        if (root->left)
            return tree_search(root->left, key);
    }

    else if (cmp_result > 0)
    {
        // switch to the right branch (if it exists)
        if (root->right)
            return tree_search(root->right, key);
    }

    else
    {
        // the keys are the same! hurray!
        return root;
    }

    return NULL; // all of the above fail. Not found.
}

// helper function. In <math.h> there's no for ints, only floats/doubles
int max(int a, int b)
{
    return a > b ? a : b;
}

// return the height of the tree (# of levels)
unsigned int tree_height(Node *root)
{
    if (root == NULL) return 0;

    return max(tree_height(root->left), tree_height(root->right)) + 1;
}

// helper function. Frees all the nodes and their keys.
void clean_tree(Node *root)
{
    if (root && root->key)
    {
        free(root->key);
        clean_tree(root->left);
        clean_tree(root->right);

        free(root);
    }
}
