#ifndef _BINARY_TREE_H_
#define _BINARY_TREE_H_

#define SIZE 100

/**************
 * STRUCTURES *
 **************/

typedef struct entry_struct Entry;
struct entry_struct
{
    char imie[SIZE + 1];
    char nazwisko[SIZE + 1];
    // char *imie;
    // char *nazwisko;
    unsigned long telefon1;
    unsigned long telefon2;
    unsigned long telefon3;
};

typedef struct node_struct Node;
struct node_struct
{
    Entry *key;
    Node *left;
    Node *right;
};


/*************
 * FUNCTIONS *
 *************/

Entry *new_entry();
Node *new_node(short);
int entry_cmp(Entry*, Entry*);
short insert_node(Node**, Node*);
void print_key(Entry *key);
void tree_LVR(Node*);
Entry* tree_min(Node*);
Entry* tree_max(Node*);
unsigned int tree_count_nodes(Node*);
Node* tree_search(Node*, Entry*);
unsigned int tree_height(Node*);
int max(int, int);
void clean_tree(Node*);

#endif
